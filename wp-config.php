<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'k|!q&A?+ah[:<*W}`W+E9R*vW$VHjUXF+6qQIMI}UKtPN]Z!E|l<!?5 ~w5:$~}%' );
define( 'SECURE_AUTH_KEY',  ';rP8sLJ LR#_84.lyV-%mEQl09T;54^3&6%6(HP,=U6S+xSYyCxLDqn#4l,~R$yD' );
define( 'LOGGED_IN_KEY',    '=l&loY3kdLc2Aj93UR$g#v%lgPZ#>0)2@z3eHhM`H$);x%(@%Dy=UQ (y:qNPg`H' );
define( 'NONCE_KEY',        'N[v2{9Qj*&~(vP@,t@Mk9f7$43~b8>KSgC5VLpZ^;8Gcp5l#`ep;BK_(ta5=?PZ;' );
define( 'AUTH_SALT',        'vbB(o)&6tn&<|A6_bUr>:r1bMg}Yc,5+<)x@aT{<Q{k2Y~srsqpRa?4Fh,hx2dT^' );
define( 'SECURE_AUTH_SALT', 'KZ6J[tr~a|)]|z1DbR^`# j1e0QX0XsK M2DEb:J~&D5}OW`H.%Z#Ea%!_=N-R1H' );
define( 'LOGGED_IN_SALT',   'Y~&yo_1^[ ~Q(eb1X<O<qoQb/PmIy^<+cFH;0CbJ?P8XFn/ZoZXjh)Gc7[i9s<?$' );
define( 'NONCE_SALT',       'eH3p!%>1|zyn|l3]Fg5=D-L]>lQCkzDh$,?7IH=:xh@KWuo_#n}xrE+Y=PCdU|%u' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
